package fr.irisa.fluxcampus.cplexStructure;

import fr.irisa.fluxcampus.tools.DuplicateMap;
import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloModeler;
import ilog.concert.IloNumVar;
import ilog.concert.IloNumVarType;

public class Arc {
	private Noeud debut;
	private Noeud fin;
	private long id;
	private int idVariable;
	
	public Arc(long id ,Noeud a , Noeud b)
	{
		this.id = id;
		setDebut(a);
		setFin(b);
	}





	public Noeud getDebut() {
		return debut;
	}



	public void setDebut(Noeud debut) {
		this.debut = debut;
	}



	public Noeud getFin() {
		return fin;
	}



	public void setFin(Noeud fin) {
		this.fin = fin;
	}
	
	public void display()
	{
		System.out.println(fin.getId());
	}
	public String toString()
	{
		return ""+fin.getId();
	}
	
	public void affichage()
	{
		System.out.println("id = " + debut.getIdVarible() + "  " + debut.getId()+ debut.getType() + "- " + this.getIdVarible() + "-->" + fin.getId() + " " + fin.getType() + " id = " + fin.getIdVarible());
	}
	public void affichageIdVariables(DuplicateMap<String, String> message)
	{
		message.put("PrettyPrint","x"+ debut.getIdVarible() +  "-- "+"e" + this.getIdVarible() + "-->" + "x"+ fin.getIdVarible());

	}
	public void initIdVariable(int id)
	{
		idVariable = id;
	}
	
	public int getIdVarible()
	{
		return idVariable;
	}
	public void affectationVariable(DuplicateMap<String, String> message, IloNumVar[] variables,char [] typeDeVariables, IloModeler modele,boolean prettyPrint){
		

					try {
						variables[idVariable] = modele.numVar(0, debut.getNombreEtudiant(),IloNumVarType.Int);
						typeDeVariables[idVariable] = 'e';
						if(prettyPrint)
							message.put("PrettyPrint","e"+idVariable  + " = [" + 0 + "," + debut.getNombreEtudiant() + "]");
					} catch (IloException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

			}
		
	

}
