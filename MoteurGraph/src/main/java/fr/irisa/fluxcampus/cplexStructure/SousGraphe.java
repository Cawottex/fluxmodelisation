package fr.irisa.fluxcampus.cplexStructure;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import fr.irisa.fluxcampus.tools.DuplicateMap;
import ilog.concert.*;
import ilog.cplex.*;
import ilog.cplex.IloCplex.UnknownObjectException;

public class SousGraphe {
	private IloNumVar[] variables;
	private Noeud entrant;
	private Noeud sortant;
	private ArrayList<Noeud> listNoeudSortant;
	private ArrayList<Noeud> listeDesNoeuds;
	private ArrayList<Arc> listeDesArcs;
	private ArrayList<ArrayList<Arc>> listeArcsEntrant;
	private ArrayList<ArrayList<Arc>> listeArcsSortant;
	private char[] tableauTypeVariable;
	private Date date;
	private IloModeler modele;
	private int accuVariable = 0;
	IloCplex model;

	String nomPromotion;

	public SousGraphe(Noeud entrant, String date, String nomPromotion) {
		this.entrant = entrant;
		listNoeudSortant = new ArrayList<Noeud>();
		listeDesNoeuds = new ArrayList<Noeud>();
		listeDesArcs = new ArrayList<Arc>();
		this.setDate(java.sql.Date.valueOf(date));
		listeDesNoeuds.add(entrant);
		listeArcsEntrant = new ArrayList<ArrayList<Arc>>();
		listeArcsSortant = new ArrayList<ArrayList<Arc>>();
		this.nomPromotion = nomPromotion;

		try {
			model = new IloCplex();
			IloCplex modele = new IloCplex();
		} catch (IloException e) {

			e.printStackTrace();
		}
		accuVariable = 1;
	}

	public Noeud getEntrant() {
		return entrant;
	}

	public void setEntrant(Noeud entrant) {
		this.entrant = entrant;
	}

	public ArrayList<Noeud> getListNoeudSortant() {
		return listNoeudSortant;
	}

	public void setListNoeudSortant(ArrayList<Noeud> listNoeudSortant) {
		this.listNoeudSortant = listNoeudSortant;
	}

	public ArrayList<Noeud> getListeDesNoeuds() {
		return listeDesNoeuds;
	}

	public void setListeDesNoeuds(ArrayList<Noeud> listeDesNoeuds) {
		this.listeDesNoeuds = listeDesNoeuds;
	}

	public void addNoeud(Noeud n) {
		listeDesNoeuds.add(n);
		n.initIdVariable(accuVariable);
		accuVariable++;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void addArc(Arc a) {
		this.listeDesArcs.add(a);
		a.initIdVariable(accuVariable);
		accuVariable++;
	}

	public void display() {
		System.out.println("Pour la date du : " + date);
		for (Noeud n : listeDesNoeuds) {
			n.display();
		}
	}

	public void afficherArc() {
		for (Arc a : listeDesArcs) {
			a.affichage();
		}
	}

	public void affichageIdVariables(DuplicateMap<String, String> message) {

		message.put("PrettyPrint", "\n\n__________Affichage Du Graphe__________\n\n");

		for (Arc a : listeDesArcs) {
			a.affichageIdVariables(message);
		}
		message.put("PrettyPrint", "\n\n__________Fin Affichage Du Graphe__________\n\n");

		message.put("PrettyPrint", "\n\n__________Affichage Arc Entrant__________\n\n");
		for (ArrayList<Arc> la : listeArcsEntrant) {
			System.out.println(la.get(0).getFin().getHeure_debut());
			for (Arc a : la) {
				a.affichageIdVariables(message);
			}
		}

		message.put("PrettyPrint", "\n\n__________FIN AFFICHAGE ENTRANT_________\n\n");

		message.put("PrettyPrint", "\n\n__________Affichage Arc Sortant__________\n\n");
		for (ArrayList<Arc> la : listeArcsSortant) {
			message.put("PrettyPrint", la.get(0).getDebut().getHeure_debut().toString());
			for (Arc a : la) {
				a.affichageIdVariables(message);
			}
		}

		message.put("PrettyPrint", "\n\n__________FIN AFFICHAGE SORTANT_________ \n\n");

	}

	public void ajouterNouvelleArcEntrant(Arc a) {

		if (listeArcsEntrant.isEmpty()) {
			ArrayList<Arc> nouvelleListe = new ArrayList<Arc>();
			nouvelleListe.add(a);
			listeArcsEntrant.add(nouvelleListe);
		} else
			chercherOuCreeNouvelleListe(a);
	}

	private boolean chercherOuCreeNouvelleListe(Arc a) {
		for (ArrayList<Arc> listeArc : listeArcsEntrant)
			if (!listeArc.isEmpty())
				if (listeArc.get(0).getFin().getHeure_debut().equals(a.getFin().getHeure_debut())) {
					listeArc.add(a);
					return true;
				}
		ArrayList<Arc> nouvelleListe = new ArrayList<Arc>();
		nouvelleListe.add(a);
		listeArcsEntrant.add(nouvelleListe);
		return true;
	}

	private boolean chercherOuCreeNouvelleListeSortant(Arc a) {
		for (ArrayList<Arc> listeArc : listeArcsSortant)
			if (!listeArc.isEmpty())
				if (listeArc.get(0).getDebut().getHeure_fin().equals(a.getDebut().getHeure_fin())) {
					listeArc.add(a);
					return true;
				}
		ArrayList<Arc> nouvelleListe = new ArrayList<Arc>();
		nouvelleListe.add(a);
		listeArcsSortant.add(nouvelleListe);
		return true;
	}

	public void ajouterNouvelleArcSortant(Arc a) {
		if (listeArcsSortant.isEmpty()) {
			ArrayList<Arc> nouvelleListe = new ArrayList<Arc>();
			nouvelleListe.add(a);
			listeArcsSortant.add(nouvelleListe);
		} else
			chercherOuCreeNouvelleListeSortant(a);
	}

	/*
	 * Il faut que la sommes des étudiants des noeuds sortant est égal à la somme
	 * des des étudiants contenues dans les arcs sortant du noeud de départ
	 */

	public Noeud searchByID(long id) {
		for (Noeud n : listeDesNoeuds)
			if (n.getId() == id)
				return n;
		return null;
	}

	// On cree un tableau de variable égal a la somme des arc et des noeuds
	public String creationCalcul(DuplicateMap<String, String> message, DuplicateMap<String, String> erreur,
			boolean prettyPrint, boolean calculSurFluxEntrant, boolean maximiser) {
		// On considére soit les flux entrant ou sortant en regardant le boolean
		// passer en paramétre
		// Si celui si est a true , alors on va maximiser et minimiser les flux
		// entrant
		// sinon même chose sur les flux sortant
		Noeud prisEnCompte;
		ArrayList<Sortie> listeDeSortie = new ArrayList<Sortie>();
		ArrayList<ArrayList<Arc>> arcPrisEnCompte;

		if (calculSurFluxEntrant) {
			arcPrisEnCompte = listeArcsSortant;
			if (prettyPrint)
				message.put("PrettyPrint", "listeArcsSortant : ");
		} else {
			arcPrisEnCompte = listeArcsEntrant;
			if (prettyPrint)
				message.put("PrettyPrint", "listeArcsEntrant : ");
		}
		if (arcPrisEnCompte == null) {
			if (prettyPrint)
				message.put("PrettyPrint", "  il est null");
		} else {
			if (prettyPrint)
				message.put("PrettyPrint", " != null");
		}
		for (ArrayList<Arc> arcNode : arcPrisEnCompte) {
			if (prettyPrint)
				message.put("PrettyPrint", "arc pris en compte :" + arcNode.size());
			String variablesString = "";
			try {
				model = new IloCplex();
				modele = new IloCplex();
				model.setOut(null);

			} catch (IloException e) {

				e.printStackTrace();
			}
			// Itération qui va maximiser et minimiser les entrées
			modele = model;
			variables = new IloNumVar[listeDesNoeuds.size() + listeDesArcs.size()];
			tableauTypeVariable = new char[listeDesNoeuds.size() + listeDesArcs.size()];
			for (Noeud n : listeDesNoeuds) {
				if (!n.equals(sortant) && !n.equals(entrant))
					n.affectationVariable(message, variables, tableauTypeVariable, modele, prettyPrint);
				else if (n.equals(sortant))
					n.affectationNoeudSortie(message, variables, tableauTypeVariable, model, prettyPrint);
				else
					n.affectationNoeudDepart(message, variables, tableauTypeVariable, modele, prettyPrint);
			}
			for (Arc a : listeDesArcs)
				a.affectationVariable(message, variables, tableauTypeVariable, modele, prettyPrint);

			// On minimise par exemple les noeuds reliées a l'entree
			try {
				IloLinearNumExpr obj = modele.linearNumExpr();
				String objectif = "";
				if (maximiser) {
					objectif = "maximiser :";
				} else
					objectif = "minimiser :";
				for (Arc a : arcNode) {
					obj.addTerm(1.0, variables[a.getIdVarible()]);
					variablesString += " e" + a.getIdVarible() + " +";
					if (prettyPrint)
						objectif += " e" + a.getIdVarible() + " +";
				}
				if (obj == null) {
					System.out.println("empty obj");
				}
				if (maximiser)
					modele.addMaximize(obj);
				else
					modele.addMinimize(obj);

				if (prettyPrint) {
					objectif = objectif.substring(0, objectif.length() - 1);
					message.put("PrettyPrint", objectif);

				}

			} catch (IloException e) {
				erreur.put("erreur", e.toString());
			}

			for (Noeud n : listeDesNoeuds)
				n.creationContrainte(message, variables, tableauTypeVariable, modele, prettyPrint);

			// On crée la dernière contrainte tq le nombre d'étudiants en sortie
			// est égal au nombre d'étudiant en entrée

			try {
				IloLinearNumExpr contr = modele.linearNumExpr();
				contr.addTerm(1, variables[entrant.getIdVarible()]);
				modele.addEq(contr, variables[sortant.getIdVarible()]);
				if (prettyPrint) {
					message.put("PrettyPrint", " {Verification etudiant entrant} == {etudiant sortiant}");
					message.put("PrettyPrint", "x" + entrant.getIdVarible() + " = x" + sortant.getIdVarible());
				}
			} catch (IloException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			// On affiche le résultat
			try {

				if (model.solve()) {

					String heureFlux;
					String heure;
					String typeSortieInOrOut;
					String effectif = Integer.toString((int) model.getObjValue());

					if (calculSurFluxEntrant) {
						if (arcNode.get(0).getDebut() == null) {
							System.out.println(" arcNode.get(0).getDebut() ) == null");
						}

						if (arcNode.get(0).getDebut().getHeure_fin().toString() == null) {
							System.out.println("arcNode.get(0).getDebut().getHeure_fin().toString() null");
						}
						heureFlux = "flux sortant a : " + arcNode.get(0).getDebut().getHeure_fin().toString() + " = ";
						heure = arcNode.get(0).getDebut().getHeure_fin().toString();
						typeSortieInOrOut = "out";
					} else {
						heureFlux = "flux entrant a : " + arcNode.get(0).getFin().getHeure_debut().toString() + " = ";
						heure = arcNode.get(0).getFin().getHeure_debut().toString();
						typeSortieInOrOut = "in";
					}
					if (prettyPrint) {
						message.put("PrettyPrint",
								"" + heureFlux + variablesString.substring(0, variablesString.length() - 1) + " = "
										+ model.getObjValue());
					}
					Sortie s = new Sortie("beaulieu", typeSortieInOrOut, date.toString(), heure, effectif, !maximiser,
							nomPromotion);
					listeDeSortie.add(s);

					/*
					 * for (int i = 0; i < 10; i++) { System.out.println(" x" + i + " = " +
					 * model.getValue(variables[i])); } System.out.println();
					 */

				} else {
					System.err.println("erreur not solve");
				}
				// model.end();

			} catch (UnknownObjectException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IloException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		String retour = "";
		for (Sortie s : listeDeSortie) {
			retour += s.toJson() + ",";

		}

		return retour;
	}

	public Noeud getSortant() {
		return sortant;
	}

	public void setSortant(Noeud sortant) {
		this.sortant = sortant;
		System.out.println("set Sortant  : " + sortant.getArcEntrant().size());
	}

	public void affectationFluxEntreeSortie() {

		for (Arc a : entrant.getArcSortant())
			ajouterNouvelleArcEntrant(a);
		for (Arc a : sortant.getArcEntrant()) {
			ajouterNouvelleArcSortant(a);
		}

	}

}
