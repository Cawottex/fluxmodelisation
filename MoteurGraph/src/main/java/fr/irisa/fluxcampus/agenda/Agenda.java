package fr.irisa.fluxcampus.agenda;

import java.util.ArrayList;

import org.neo4j.graphdb.Node;

import fr.irisa.fluxcampus.hierachie_universitaire.Promotion;
import model.getEvenNonParse.EvenementParse;


public class Agenda {
	private ArrayList<Jour> listeDesJours = new ArrayList<Jour>();
	private Promotion promotion;


	public Agenda(Promotion p){
		promotion = p;
	}
	
	public ArrayList<Jour> getListeDesJours() {
		return listeDesJours;
	}

	public void setListeDesJours(ArrayList<Jour> listeDesJours) {
		this.listeDesJours = listeDesJours;
	}
	
	public void insererCours(EvenementParse e)
	{

		//Si la date de l'evenement correspond la methode existInListRajouterEvenement , rajoute automatiquemenet l'evenement au jour correspondant
		//sinon renvoie false
			if(!Jour.existInListRajouterEvenement(listeDesJours,e))
			{
				Jour j = new Jour(e,this);
				listeDesJours.add(j);
			}
		
	}

	public void afficherAgenda() {
		
		for(Jour j : listeDesJours)
		{
			j.affichageAgenda();
		}
	}

	public void trier() {
		listeDesJours.sort(Jour.sortDate);
		for(Jour j : listeDesJours)
		{
			j.trier();
		}
		
	}

	//TODO
	public void creationGraph() {
		
		for(Jour j : listeDesJours)
		{
			j.creationGraph();
		}
		
	}

	public Promotion getPromotion() {
		return promotion;
	}


	public Node getNodePromotion()
	{
		return this.getPromotion().getNode();
	}
	
	
	

}
