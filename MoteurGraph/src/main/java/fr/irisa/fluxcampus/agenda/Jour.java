package fr.irisa.fluxcampus.agenda;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.neo4j.graphdb.Node;

import fr.irisa.fluxcampus.MoteurGraph.TransactionGraph;
import fr.irisa.fluxcampus.MoteurGraph.TypeDeNoeud;
import fr.irisa.fluxcampus.tools.Tools;
import model.getEvenNonParse.*;
import model.getInfoUE.InfoUE;
import model.libellecours.LibelleCours;

public class Jour {
	private ArrayList<HeureDeCours> listeCours = new ArrayList<HeureDeCours>();
	private Date date;
	private Agenda agenda;
	private Node nodeDate;

	Jour(Date date, EvenementParse evenement, Agenda a) {
		this.setDate(date);
		HeureDeCours h = new HeureDeCours(evenement);
		listeCours.add(h);
		agenda = a;
	}

	Jour(EvenementParse e, Agenda a) {
		this.setDate(Date.valueOf(e.getDate()));
		HeureDeCours h = new HeureDeCours(e);
		listeCours.add(h);
		agenda = a;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public ArrayList<HeureDeCours> getList() {
		return listeCours;
	}

	public boolean memeDate(Date dateCompare) {
		return (date.compareTo(dateCompare) == 0);
	}

	public void insererCours(HeureDeCours e) {
		listeCours.add(e);
	}

	public void insererCours(EvenementParse e) {
		HeureDeCours h = new HeureDeCours(e);
		boolean memeTempsTrouve = false;
		for (HeureDeCours he : listeCours) {
			if (he.heureMemeMomement(h)) {
				he.rajouterHeureDansListe(h);
				memeTempsTrouve = true;
				break;
			}
		}
		if (!memeTempsTrouve)
			listeCours.add(h);
	}

	public static boolean existInListRajouterEvenement(ArrayList<Jour> liste, EvenementParse e) {
		Date d = Date.valueOf(e.getDate());
		for (Jour j : liste) {
			if (j.memeDate(d)) {
				j.insererCours(e);
				return true;
			}
		}
		return false;
	}

	public void affichageAgenda() {
		System.out.println("Affichage des cours pour le jour : " + date.toString());
		for (HeureDeCours h : listeCours) {
			h.affichageAgenda();

		}
	}

	/* Comparator for sorting the list by Hour */
	public static Comparator<Jour> sortDate = (s1, s2) -> {
		return (int) (s1.date.compareTo(s2.getDate()));
	};

	public void trier() {
		listeCours.sort(HeureDeCours.sortHour);

	}

	public TypeDeNoeud getTypeDeNoeud(HeureDeCours h) {
		TypeDeNoeud typeDeN = null;
		String typeRecupere = h.getType_cours();
		if (TypeDeNoeud.typeDifferent(typeRecupere)) {
			typeDeN = TypeDeNoeud.createTypeDifferents(typeRecupere);
		} else {
			typeDeN = TypeDeNoeud.createTypeDeNoeud(typeRecupere);
		}
		return typeDeN;
	}

	public int getNombreEtudiantDansNoeud(HeureDeCours h) {
		int nombre_etudiant_noeud = 0;
		String typeRecupere = h.getType_cours();
		if (TypeDeNoeud.typeDifferent(typeRecupere)) {
			nombre_etudiant_noeud = agenda.getPromotion().getNombrePersonneDansGroupe(h.getGroupe());
		} else {
			nombre_etudiant_noeud = agenda.getPromotion().getNombre_etudiant();
		}
		return nombre_etudiant_noeud;
	}

	public void creationGraphPlusieursUE(Node noeudDate, int nombre_etudiant_promotion) {
		System.err.println("rentre plusieurs ue");
		HeureDeCours premiereHeure = listeCours.get(0);
		TransactionGraph transaction = agenda.getPromotion().getGraph();
		boolean promotionAuComplet = false;
		ArrayList<Node> listeNodeEtudiantLibre = new ArrayList<Node>();
		TypeDeNoeud typeDeN = getTypeDeNoeud(premiereHeure);

		// On recupere la liste des UE
		ArrayList<InfoUE> listeInfoUE = agenda.getPromotion().getListeInformationUE();
		// On recupere le nombre d'étudiant
		int nombre_etudiant_noeud;
		if (!premiereHeure.getGroupe().isEmpty()) {
			System.out.println("getGroupe est != empty");
			System.out.println("taille liste ue : " + listeInfoUE.size());
			nombre_etudiant_noeud = (int) (nombre_etudiant_promotion
					* Tools.typeUEdansHeure(listeInfoUE, premiereHeure));
			System.out.println("typeHEUREnOMBRE : " +  Tools.typeUEdansHeure(listeInfoUE, premiereHeure));
			
		}
		else
			nombre_etudiant_noeud = getNombreEtudiantDansNoeud(premiereHeure);
		if(nombre_etudiant_noeud == nombre_etudiant_promotion) {
			promotionAuComplet = true;
		}
			
		// Création du premier noeud
		Node n = transaction.addNode(typeDeN, typeDeN.getName());
		transaction.setNombreEtudiantDansNoeud(n,
				 nombre_etudiant_noeud);
		transaction.addNodeProperty(n, "EtudiantPresent", "" + nombre_etudiant_noeud);
		premiereHeure.rajouterLibelleAuNoeud(transaction, n);
		transaction.creationRelationContiensAvecNombreEtudiant(nodeDate, n, nombre_etudiant_noeud);
		transaction.setNombreEtudiantDansNoeud(nodeDate,
				transaction.getNombreEtudiantDansNoeud(nodeDate) - nombre_etudiant_noeud);
		listeNodeEtudiantLibre.add(n);
		for (int i = 1; i < listeCours.size(); i++) {
			HeureDeCours heureCourante = listeCours.get(i);
			typeDeN = getTypeDeNoeud(heureCourante);
			if (!premiereHeure.getGroupe().isEmpty())
				nombre_etudiant_noeud = (int) (nombre_etudiant_promotion
						* Tools.typeUEdansHeure(listeInfoUE, premiereHeure));
			else
				nombre_etudiant_noeud = getNombreEtudiantDansNoeud(premiereHeure);
			System.out.println("nombre_etudiant_noeud = " + nombre_etudiant_noeud +  " nombre_etudiant_promotion " + nombre_etudiant_promotion);
			if(nombre_etudiant_noeud == nombre_etudiant_promotion) {
				System.out.println("etudiant promo compelt");
				promotionAuComplet = true;
			}
			n = transaction.addNode(typeDeN, typeDeN.getName());

			transaction.setNombreEtudiantDansNoeud(n, nombre_etudiant_noeud);

			transaction.addNodeProperty(n, "EtudiantPresent", "" + nombre_etudiant_noeud);
			System.out.println("Premier noeud  promotionAuComplet = " + promotionAuComplet);

			//Plusieurs possiblités soit le noeud contient touts les etudiants de la promotion
			//Donc c'est le cas le plus trivial, on relie touts les noeuds de la liste a celui-ci puis on les supprime et rajoute le dernier noeuds
			//Soit il ne contient pas tous les etudiants
			//1) la liste > 1 || == 1
			//Si tout les etudiants sont rentrés alors 
;
			
			//Si le noeud courant contient tous les étudiants
			if(nombre_etudiant_noeud == nombre_etudiant_promotion)
			{
				System.err.println("neoud : " + i + " rentre dans I");
				//Si la promotion est au complet
				if(promotionAuComplet)
				{
					System.out.println("rentre dans promotionComplet");
					transaction.relierNoeud(n,listeNodeEtudiantLibre);
					transaction.viderEtudiantListe(listeNodeEtudiantLibre);
					listeNodeEtudiantLibre.clear();
					listeNodeEtudiantLibre.add(n);
				}
				//sinon
				else
				{
					System.out.println("NON rentre dans promotionComplet");

					transaction.relierNoeud(n,listeNodeEtudiantLibre);
					//on le relie au noeud de depart
					transaction.creationRelationContains(noeudDate, n);	
					listeNodeEtudiantLibre.clear();
					listeNodeEtudiantLibre.add(n);
				}
				
				promotionAuComplet = true;
			}
			else
			{
				System.err.println("neoud : " + i + " rentre dans II");

				//Si la promotion est au complet
				if(promotionAuComplet)
				{
					System.out.println("rentre dans promotionComplet");

					//Alors on relie ce noeud a tous les noeuds de la liste et on le rajoute
					transaction.relierNoeud(n,listeNodeEtudiantLibre);
					listeNodeEtudiantLibre.add(n);
					
				}
				else
				{
					System.out.println("Non rentre dans promotionComplet");

					transaction.relierNoeud(n,listeNodeEtudiantLibre);
					//on le relie au noeud de depart
					transaction.creationRelationContains(noeudDate, n);	
					listeNodeEtudiantLibre.add(n);
				}
			}
			transaction.setNombreEtudiantDansNoeud(n,
					 nombre_etudiant_noeud);
			transaction.addNodeProperty(n, "EtudiantPresent", "" + nombre_etudiant_noeud);
			heureCourante.rajouterLibelleAuNoeud(transaction, n);
		}
	}

	public void creationGraphUneListeUEPlusieursGroupes(Node noeudDate, int nombre_etudiant_Promotion) {

		TransactionGraph transaction = agenda.getPromotion().getGraph();
		HeureDeCours premiereHeure = listeCours.get(0);
		int nombre_etudiant_noeud = getNombreEtudiantDansNoeud(premiereHeure);
		// On recupere la liste des UE
		ArrayList<InfoUE> listeInfoUE = agenda.getPromotion().getListeInformationUE();
		if (!premiereHeure.getGroupe().isEmpty())
			nombre_etudiant_noeud = (int) (nombre_etudiant_Promotion
					* Tools.typeUEdansHeure(listeInfoUE, premiereHeure));
		else
			nombre_etudiant_noeud = getNombreEtudiantDansNoeud(premiereHeure);
		ArrayList<Node> listeNodeEtudiantLibre = new ArrayList<Node>();
		TypeDeNoeud typeDeN = getTypeDeNoeud(premiereHeure);

		Node n = transaction.addNode(typeDeN, typeDeN.getName());
		premiereHeure.rajouterLibelleAuNoeud(transaction, n);

		// On relie au noeud de la date
		System.out.println("premier noeud : " + nombre_etudiant_noeud );
		transaction.creationRelationContiensAvecNombreEtudiant(nodeDate, n, nombre_etudiant_noeud);
		// On récupere dans le noeud de départ le nombre d'étudiants

		transaction.setNombreEtudiantDansNoeud(n, nombre_etudiant_noeud);
		transaction.setNombreEtudiantDansNoeud(nodeDate,
				transaction.getNombreEtudiantDansNoeud(nodeDate) - nombre_etudiant_noeud);
		transaction.addNodeProperty(n, "EtudiantPresent", "" + nombre_etudiant_noeud);
		listeNodeEtudiantLibre.add(n);
		for (int i = 1; i < listeCours.size(); i++) {
			System.out.println("noeud :" + i);
			HeureDeCours heureCourtante = listeCours.get(i);
			typeDeN = getTypeDeNoeud(heureCourtante);
			System.out.println("Noeud normalement cree");
			if (!premiereHeure.getGroupe().isEmpty())
				nombre_etudiant_noeud = (int) (nombre_etudiant_Promotion
						* Tools.typeUEdansHeure(listeInfoUE, heureCourtante));
			else
				nombre_etudiant_noeud = getNombreEtudiantDansNoeud(heureCourtante);
			n = transaction.addNode(typeDeN, typeDeN.getName());
			System.out.println("nombre etudiant :" + nombre_etudiant_noeud);
			int nombreEtudiantPossibleDernierNoeud = transaction
					.getNombreEtudiantsLibreDansListe(listeNodeEtudiantLibre);
			System.out.println("nombreEtudiantPossibleDernierNoeud = " + nombreEtudiantPossibleDernierNoeud);
			if (nombreEtudiantPossibleDernierNoeud < nombre_etudiant_noeud) {
				int nombreEtudiantVoulueEncore = nombre_etudiant_noeud - nombreEtudiantPossibleDernierNoeud;
				int nombreEtudiantPromotionToujoursPasRentree = transaction.getNombreEtudiantDansNoeud(nodeDate);
				if (nombreEtudiantVoulueEncore > nombreEtudiantPromotionToujoursPasRentree) {
					System.err.println("ERREUR L171 JOUR" + this.date.toString());
					// System.exit(-1);
				}
				// On cree un lien entre le noeud courant et le
				// noeud de date et on enleve le nombre d'étudiants
				// pris au noeud de date
				transaction.creationRelationContiensAvecNombreEtudiant(nodeDate, n, nombreEtudiantVoulueEncore);
				transaction.setNombreEtudiantDansNoeud(nodeDate,
						nombreEtudiantPromotionToujoursPasRentree - nombreEtudiantVoulueEncore);
				transaction.setNombreEtudiantDansNoeud(n, nombre_etudiant_noeud);
				transaction.addNodeProperty(n, "EtudiantPresent", "" + nombre_etudiant_noeud);
				nombre_etudiant_noeud = nombre_etudiant_noeud - nombreEtudiantVoulueEncore;

			} else {
				transaction.setNombreEtudiantDansNoeud(n, nombre_etudiant_noeud);
				transaction.addNodeProperty(n, "EtudiantPresent", "" + nombre_etudiant_noeud);
			}
			transaction.creationRelationContientNombreEtudiant(listeNodeEtudiantLibre, n, nombre_etudiant_noeud);
			heureCourtante.rajouterLibelleAuNoeud(transaction, n);
			listeNodeEtudiantLibre.add(n);

		}

	}

	public void creationGraph() {
		int nombre_etudiant_dans_promotion = agenda.getPromotion().getNombre_etudiant();
		TransactionGraph transaction = agenda.getPromotion().getGraph();
		TypeDeNoeud typeDate = TypeDeNoeud.createTypeDeNoeud(this.getDate());
		nodeDate = transaction.addNode(typeDate, this.date.toString());
		Node nodePromotion = agenda.getNodePromotion();
		// On relie le noeud de la promotion et celui de la date
		transaction.creationRelationContains(nodePromotion, nodeDate);
		transaction.setNombreEtudiantDansNoeud(nodeDate, nombre_etudiant_dans_promotion);
		String nomPromotion = agenda.getPromotion().getName();
		transaction.addNodeProperty(nodeDate, "promotion", nomPromotion);
		transaction.addNodeProperty(nodeDate, "EtudiantPresent", "" + nombre_etudiant_dans_promotion);
		//this.creationGraphPlusieursUE(nodeDate, nombre_etudiant_dans_promotion);
		// On regarde les différentes liste d'UE et les groupes

		ArrayList<InfoUE> listeInfoUE = agenda.getPromotion().getListeInformationUE();
		int tailleListe = listeInfoUE.size();
		System.out.println("taille liste UE : " + tailleListe);
		if ((tailleListe == 1 || tailleListe == 0) && agenda.getPromotion().getListesDesGroupes().size() == 0) {
			this.creationGraphPlusieursUE(nodeDate, nombre_etudiant_dans_promotion);
			/*
			if (tailleListe == 0 || listeInfoUE.get(0).getNombre_ue_a_choisire()
					.equals(listeInfoUE.get(0).getNombre_ue_disponible())) {
				int nombre_etudiant_noeud = agenda.getPromotion().getNombre_etudiant();

				TypeDeNoeud typeDeN = null;
				String typeRecupere = listeCours.get(0).getType_cours();
				if (TypeDeNoeud.typeDifferent(typeRecupere)) {
					String groupe = listeCours.get(0).getGroupe();
					nombre_etudiant_noeud = agenda.getPromotion().getNombrePersonneDansGroupe(groupe);
					typeDeN = TypeDeNoeud.createTypeDifferents(typeRecupere);
				} else {
					nombre_etudiant_noeud = agenda.getPromotion().getNombre_etudiant();
					typeDeN = TypeDeNoeud.createTypeDeNoeud(typeRecupere);
				}
				Node n = transaction.addNode(typeDeN, typeDeN.getName());
				listeCours.get(0).rajouterLibelleAuNoeud(transaction, n);
				Node dernierNoeud = n;
				// On relie au noeud de la date
				System.out.println("taille liste cours :" + listeCours.size());
				transaction.creationRelationContiensAvecNombreEtudiant(nodeDate, n, nombre_etudiant_noeud);
				// On récupere dans le noeud de départ le nombre d'étudiants
				// pris
				transaction.setNombreEtudiantDansNoeud(dernierNoeud,
						nombre_etudiant_dans_promotion - nombre_etudiant_noeud);
				transaction.setNombreEtudiantDansNoeud(n, nombre_etudiant_noeud);
				transaction.setNombreEtudiantDansNoeud(nodeDate,
						transaction.getNombreEtudiantDansNoeud(nodeDate) - nombre_etudiant_noeud);
				transaction.addNodeProperty(n, "EtudiantPresent", "" + nombre_etudiant_noeud);

				for (int i = 1; i < listeCours.size(); i++) {
					typeRecupere = listeCours.get(i).getType_cours();
					if (TypeDeNoeud.typeDifferent(typeRecupere)) {
						String groupe = listeCours.get(i).getGroupe();
						nombre_etudiant_noeud = agenda.getPromotion().getNombrePersonneDansGroupe(groupe);
						typeDeN = TypeDeNoeud.createTypeDifferents(typeRecupere);
					} else {
						nombre_etudiant_noeud = agenda.getPromotion().getNombre_etudiant();
						typeDeN = TypeDeNoeud.createTypeDeNoeud(typeRecupere);
					}
					System.out.println("Noeud normalement cree");

					n = transaction.addNode(typeDeN, typeDeN.getName());
					int nombreEtudiantPossibleDernierNoeud = transaction.getNombreEtudiantDansNoeud(dernierNoeud);
					if (nombreEtudiantPossibleDernierNoeud < nombre_etudiant_noeud) {
						int nombreEtudiantVoulueEncore = nombre_etudiant_noeud - nombreEtudiantPossibleDernierNoeud;
						int nombreEtudiantPromotionToujoursPasRentree = transaction
								.getNombreEtudiantDansNoeud(nodeDate);
						if (nombreEtudiantVoulueEncore > nombreEtudiantPromotionToujoursPasRentree) {
							System.err.println("ERREUR L171 JOUR" + this.date.toString());
							// System.exit(-1);
						}
						// On cree un lien entre le noeud courant et le
						// noeud de date et on enleve le nombre d'étudiants
						// pris au noeud de date
						transaction.creationRelationContiensAvecNombreEtudiant(nodeDate, n, nombreEtudiantVoulueEncore);
						transaction.setNombreEtudiantDansNoeud(nodeDate,
								nombreEtudiantPromotionToujoursPasRentree - nombreEtudiantVoulueEncore);
						transaction.setNombreEtudiantDansNoeud(n, nombre_etudiant_noeud);
						transaction.addNodeProperty(n, "EtudiantPresent", "" + nombre_etudiant_noeud);
						nombre_etudiant_noeud = nombre_etudiant_noeud - nombreEtudiantVoulueEncore;

					} else {
						transaction.setNombreEtudiantDansNoeud(n, nombre_etudiant_noeud);
						transaction.addNodeProperty(n, "EtudiantPresent", "" + nombre_etudiant_noeud);
					}
					transaction.creationRelationContiensAvecNombreEtudiant(dernierNoeud, n, nombre_etudiant_noeud);
					listeCours.get(i).rajouterLibelleAuNoeud(transaction, n);
					int nombre_etudiant_dernierNoeud = transaction.getNombreEtudiantDansNoeud(dernierNoeud);
					transaction.setNombreEtudiantDansNoeud(dernierNoeud,
							nombre_etudiant_dernierNoeud - nombre_etudiant_noeud);
					if (tailleListe == 1) {
						Pattern p = Pattern.compile(listeInfoUE.get(0).getTag());
						Matcher mDescription = p.matcher(listeCours.get(i).getDescription());
						Matcher mSummary = p.matcher(listeCours.get(i).getSummary());
						boolean dejaTrouve = false;
						if (mDescription.find()) {
							dejaTrouve = true;
							for (LibelleCours s : agenda.getPromotion().getListeLibelleCours()) {
								Pattern patternLibelle = Pattern.compile(s.getLibelle_cours());
								Matcher mLibelle = patternLibelle.matcher(listeCours.get(i).getDescription());
								if (mLibelle.find()) {
									transaction.addNodeProperty(n, "UE", s.getLibelle_cours());

								}
								break;
							}
						}

						if (!dejaTrouve && mSummary.find()) {
							for (LibelleCours s : agenda.getPromotion().getListeLibelleCours()) {
								Pattern patternLibelle = Pattern.compile(s.getLibelle_cours());
								Matcher mLibelle = patternLibelle.matcher(listeCours.get(i).getSummary());
								if (mLibelle.find()) {
									transaction.addNodeProperty(n, "UE", s.getLibelle_cours());

								}
								break;
							}
						}
					}
					dernierNoeud = n;

				}
				
			}
			*/
		} else if ((tailleListe == 1 || tailleListe == 0) && agenda.getPromotion().getListesDesGroupes().size() != 0) {
			this.creationGraphPlusieursUE(nodeDate, nombre_etudiant_dans_promotion);
		} else if (tailleListe > 1)
			this.creationGraphPlusieursUE(nodeDate, nombre_etudiant_dans_promotion);
	}

}
