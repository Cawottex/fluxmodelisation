package fr.irisa.fluxcampus.agenda;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Comparator;

import org.neo4j.graphdb.Node;

import fr.irisa.fluxcampus.MoteurGraph.TransactionGraph;
import model.getEvenNonParse.EvenementParse;

public class HeureDeCours {
	private String summary;
	private String description;
	private String localisation;
	private String groupe;
	private String type_cours;
	private Date date;
	private Time heureDebut;
	private Time heureFin;
	private String location;
	private ArrayList<HeureDeCours> listeHeureMemeTemps = new ArrayList<HeureDeCours>();

	public HeureDeCours(EvenementParse e) {
		setSummary(e.getSummary());
		setDescription(e.getDescription());
		setLocalisation(e.getLocalisation());
		setGroupe(e.getGroupe());
		setType_cours(e.getType_cours());
		setDate(Date.valueOf(e.getDate()));
		setHeureDebut(Time.valueOf(e.getHeureDebut()));
		setHeureFin(Time.valueOf(e.getHeureFin()));
		setLocation(e.getLocation());
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getType_cours() {
		return type_cours;
	}

	public void setType_cours(String type_cours) {
		this.type_cours = type_cours;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Time getHeureDebut() {
		return heureDebut;
	}

	public void setHeureDebut(Time heureDebut) {
		this.heureDebut = heureDebut;
	}

	public Time getHeureFin() {
		return heureFin;
	}

	public void setHeureFin(Time heureFin) {
		this.heureFin = heureFin;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getGroupe() {
		return groupe;
	}

	public void setGroupe(String groupe) {
		this.groupe = groupe;
	}

	public String getLocalisation() {
		return localisation;
	}

	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}

	public void affichageAgenda() {
		System.out.println(
				"heure debut :" + this.getHeureDebut().toString() + " heure fin : " + this.getHeureFin().toString());
		if(this.getListeHeureMemeTemps().size() != 0)
		{
			System.out.println("heure se passant en même temps : " );
			for(HeureDeCours el : getListeHeureMemeTemps())
			{
				el.affichageAgenda();
			}
		}
	}

	/* Comparator for sorting the list by Hour */
	public static Comparator<HeureDeCours> sortHour = (s1, s2) -> {
		return (int) (s1.getHeureDebut().getTime() - s2.getHeureDebut().getTime());
	};

	// Renvoie true si l'objet et l'heure passée en paramètre partage la même
	// zone horraire
	// par exemple :
	// HeureDeCours x1
	// HeureDeCours x2
	// Si x1.debut = x2.debut || x1.fin = x2.fin || x1.debut.EstAvant
	/**
	 * Trouver groupe incompatible.
	 */
	public boolean heureMemeMomement(HeureDeCours a) {
		HeureDeCours h = this;
		if ((h.getHeureDebut().equals(a.getHeureDebut())) || (h.getHeureFin().equals(a.getHeureFin()))
				|| (h.getHeureDebut().after(a.getHeureDebut()) && h.getHeureFin().before(a.getHeureFin()))
				|| (a.getHeureDebut().after(h.getHeureDebut()) && a.getHeureFin().before(h.getHeureFin()))
				|| (a.getHeureDebut().before(h.getHeureDebut()) && h.getHeureFin().before(a.getHeureFin()))
				|| (h.getHeureDebut().before(a.getHeureDebut()) && a.getHeureFin().before(h.getHeureFin()))
				|| (a.getHeureDebut().before(h.getHeureDebut()) && h.getHeureFin().before(a.getHeureFin()))
				|| (h.getHeureDebut().before(a.getHeureDebut()) && h.getHeureFin().after(a.getHeureFin()))
				|| (a.getHeureDebut().before(h.getHeureDebut()) && a.getHeureFin().after(h.getHeureDebut()))
				|| (h.getHeureDebut().before(a.getHeureDebut()) && h.getHeureFin().before(a.getHeureFin())
						&& h.getHeureFin().after(a.getHeureDebut()))
				||

				(a.getHeureDebut().before(h.getHeureDebut()) && a.getHeureFin().before(h.getHeureFin())
						&& a.getHeureFin().after(h.getHeureDebut()))

		) {

			return true;
		} else
			return false;

	}

	public ArrayList<HeureDeCours> getListeHeureMemeTemps() {
		return listeHeureMemeTemps;
	}

	public void setListeHeureMemeTemps(ArrayList<HeureDeCours> listeHeureMemeTemps) {
		this.listeHeureMemeTemps = listeHeureMemeTemps;
	}
	
	public void rajouterHeureDansListe(HeureDeCours h)
	{
		listeHeureMemeTemps.add(h);
	}
	
	public void rajouterLibelleAuNoeud(TransactionGraph transaction,Node n)
	{
		transaction.addNodeProperty(n,"Heure_fin", this.getHeureFin().toString());
		transaction.addNodeProperty(n,"Heure_debut", this.getHeureDebut().toString());
		transaction.addNodeProperty(n,"summary", this.getSummary());
		transaction.addNodeProperty(n,"description", this.getDescription());
		transaction.addNodeProperty(n, "localisation", this.getLocalisation());

	}

}
