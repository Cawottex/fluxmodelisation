package fr.irisa.fluxcampus.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import fr.irisa.fluxcampus.hierachie_universitaire.Formation;
import fr.irisa.fluxcampus.hierachie_universitaire.Promotion;
import fr.irisa.fluxcampus.structure.FichierBdd;
import model.getInfoUE.InfoUE;

public class ToolsAffichage {

	public static final void affichageListeFichierBdd(ArrayList<FichierBdd> liste) {
		for (FichierBdd f : liste) {
			System.out.println("fichier : " + f.getName() + " formation : " + f.getFormation() + " promo : "
					+ f.getPromo() + " annee_scolaire  : " + f.getAnnee_scolaire());
		}
	}

	public static final void affichageFormation(ArrayList<Formation> liste) {
		for (Formation f : liste) {
			System.out.println("Formation : " + f.getNom());
			ToolsAffichage.affichagePromotion(f.getListeDesPromotionDeLaFormation());

		}
	}

	public static final void affichagePromotion(ArrayList<Promotion> liste) {
		for (Promotion p : liste) {
			System.out.println(
					"Nom de la promotion : " + p.getName() + " nombre d'étudiants : " + p.getNombre_etudiant());
			System.out.println(p.getListeInformationUE());
		}
	}

	public static void affichageUE(ArrayList<InfoUE> liste) {
		for (InfoUE i : liste) {
			System.out.println("UE : " + i.getTag() + " nombre à prendre : " + i.getNombre_ue_a_choisire() + " sur : "
					+ i.getNombre_ue_disponible());
			;
		}
	}

	public static String arrayListToString(ArrayList<String> liste) {
		String retour = "";
		for (String s : liste)
			retour += s + "\n";
		return retour;
	}

	public static final String affichageErreur(HashMap<String, String> map) {
		String retour = "";
		Set<Entry<String, String>> entrySet = map.entrySet();
		Iterator<Entry<String, String>> it = entrySet.iterator();
		while (it.hasNext()) {
			Map.Entry me = (Map.Entry) it.next();
			retour += me.getKey() + " message : " + me.getValue() +" \n";

		}
		return retour;
	}
	
	
	
	
	

}
