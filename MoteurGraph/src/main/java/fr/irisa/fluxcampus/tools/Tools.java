package fr.irisa.fluxcampus.tools;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.irisa.fluxcampus.agenda.HeureDeCours;
import model.getInfoUE.InfoUE;

public class Tools {

	public static double typeUEdansHeure(ArrayList<InfoUE> liste, HeureDeCours h) {
		for (InfoUE info : liste) {
			info.setTag(info.getTag().toUpperCase());
			System.out.println("ue tag : " + info.getTag());
			Pattern p = Pattern.compile(info.getTag());
			Matcher mDescription = p.matcher(h.getDescription());
			Matcher mSummary = p.matcher(h.getSummary());
			if (mSummary.find() || mDescription.find()) {
				System.out.println("trouve avec ue : " + info.getTag());
				System.out.println("nombre ue a choisire :" +  info.getNombre_ue_a_choisire());
				System.out.println("getNombre_ue_disponiblechoisire :" +  info.getNombre_ue_disponible());
				return (1.0* Integer.valueOf(info.getNombre_ue_a_choisire())
						/ (Integer.valueOf(info.getNombre_ue_disponible())* 1.0));
				
			}
		}
		if(liste.isEmpty())
			return 1;
		System.out.println("non trouve  ue : ");
		return -1;
	}
}
