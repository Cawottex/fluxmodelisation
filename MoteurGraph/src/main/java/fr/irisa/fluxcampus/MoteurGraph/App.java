package fr.irisa.fluxcampus.MoteurGraph;

import java.util.ArrayList;
import java.util.Collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irisa.fluxcampus.hierachie_universitaire.Campus;
import fr.irisa.fluxcampus.hierachie_universitaire.Formation;
import fr.irisa.fluxcampus.hierachie_universitaire.Promotion;
import fr.irisa.fluxcampus.interfaceAPI.RecuperationInformation;
import fr.irisa.fluxcampus.structure.FichierBdd;
import fr.irisa.fluxcampus.tools.ReaderCSV;
import fr.irisa.fluxcampus.tools.ToolsAffichage;

/**
 * Hello world!
 *
 */
public class App {
	private static TransactionGraph transactionGraph;
	private static RecuperationInformation recuperation;
	private static final Logger LOG = LogManager.getLogger(App.class);
	/*
	private static String[] fichierDisponible = { "Informatique_L2_S3_G1", "Informatique_L2_S3_G2",
			"Informatique_L2_S3_G3", "Informatique_L2_S3_G4", "Informatique_L2_S3_G5","SVE_S1_L1_GR05","SVE_S1_GR04(LV2)","SVE_S1_L1_GR01a(SI-LV2)",
			"SVE_S1_L1_GR03(LV2)","SVE_S1_L1_GR06","SVE_S1_L1_GR07","SVE_S1_L1_GR08","SVE_S1_L1_GR09","SVE_S1_L1_GR10","SVE_S1_L1_GR11"
			,"SVE_S1_L1_GR11","SVE_S1_L1_GR13",};
*/
	public static void main(String[] args) throws InterruptedException {
		/*
		System.out.println("BONJOURRR ");
		ReaderCSV reader = new ReaderCSV("/home/cawo/Bureau/fichierICS.csv");
		ArrayList<String>fichierDisponible= reader.getListeCSV();
		int n = 165;
		int m = 170;
/*
 * Modifier 		pour x de n,m
 * 
 * GEII BR2
 */
		/*
		for(;n <m;n++)
		{
			String fichier = fichierDisponible.get(n);
			
		//for (String fichier : fichierDisponible) {
			ArrayList<Formation> listeFormation = new ArrayList<Formation>();
			String fichier = "Mathematiques_L1_GROUPEMA1";
			String fichierAvecExtension = fichier + ".ics";
			String dossier = Chemin.graphDossierSortie;
			String date = null;
			boolean envoieToAPI = false;
			boolean prettyPrintCplx = true;
		//	Chemin.graphFichier = dossier + fichier;
			transactionGraph = new TransactionGraph(fichier);
			System.out.println("info 1");
			recuperation = new RecuperationInformation();
			System.out.println("info 2");
			ArrayList<FichierBdd> listeFichier = recuperation.getAllFichier();
			// fr.irisa.fluxcampus.tools.ToolsAffichage.affichageListeFichierBdd(listeFichier);

			System.out.println("info 2.5");
			Campus beaulieu = new Campus("Beaulieu", transactionGraph);
			// Recuperation FichierBdd (Nom_promo Nom_Formation Nom_Fichier)

			System.out.println("info 3");
			System.out.println("fichier = "+ fichierAvecExtension );
			FichierBdd f = FichierBdd.rechercheDansListe(listeFichier, fichierAvecExtension, "2017");
			if (f == null) {
				System.out.println("fichier null");
			}
			System.out.println("info 4");
			beaulieu.rajouterPromotion(f);
			System.out.println("info 4.5");
			beaulieu.recupererInformation(date);
			beaulieu.recupererInformationNiveau(fichierAvecExtension);
			System.out.println("info 5");

			beaulieu.creeAgenda();
			System.out.println("info 6");

			beaulieu.creationGraph();

			transactionGraph.creation_Node_End();

			System.out.println("Programme fini");
			// transactionGraph.searchInData();
			transactionGraph.calculSimplexBis(false, envoieToAPI);
			*/
		Application2 app = new Application2("~/Bureau/", false, false, null);
		app.runFichier("SVE_L2_BO3E_S3_GR3_LV2");
		
		}
	
}
