package fr.irisa.fluxcampus.MoteurGraph;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import fr.irisa.fluxcampus.tools.ReaderCSV;
import fr.irisa.fluxcampus.tools.ToolsAffichage;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class InitialisationInformationView {
	@FXML
	private TextArea affichageText;

	@FXML
	private Button afficherFichiersDisponible;

	@FXML
	private TextField textSortieCheminGraph;

	@FXML
	private CheckBox prettyPrintYes;
	@FXML
	private CheckBox prettyPrintNo;

	@FXML
	private CheckBox envoieToAPIYes;
	@FXML
	private CheckBox envoieToAPINo;
	@FXML
	private DatePicker date;

	private HashMap<String, String> erreur = new HashMap<String, String>();

	// Reference à l'application principale
	private InterfaceGraphique mainApp;

	/*
	 * Initialise le contrôleur de classe,cette méthode est automatiquement appeler
	 * après que le fichier XML soit chargé.
	 */
	@FXML
	private void initialize() {

	}

	public void setMainApp(InterfaceGraphique mainApp) {
		this.mainApp = mainApp;
	}

	@FXML
	private void pressOnListerFichiers() {
		ReaderCSV reader = new ReaderCSV("/home/cawo/Bureau/fichierICS.csv");
		ArrayList<String> listefichier = reader.getListeCSV();
		affichageText.setText(ToolsAffichage.arrayListToString(listefichier));

	}

	@FXML
	private void pressOnConfirmer() {

		// On récupères les valeurs des champs

		boolean prettyPrintYesBoolean = prettyPrintYes.isSelected();
		boolean prettyPrintNoBoolean = prettyPrintNo.isSelected();
		boolean envoieToAPINoBoolean = envoieToAPINo.isSelected();
		boolean envoieToAPIYesBoolean = envoieToAPIYes.isSelected();
		boolean envoie = false;
		boolean prettyPrint = false;
		String recuperationChemin = textSortieCheminGraph.getText();
		String dateS = null;
		if (date.getValue() != null)
			dateS = date.getValue().toString();

		if (prettyPrintYesBoolean != true && prettyPrintNoBoolean != true
				|| prettyPrintYesBoolean == prettyPrintNoBoolean) {
			// on rajoute une erreur
			ajoutErreur("PrettyPrint", "Erreur un et seulement un des paramètres doit être coché");
		}

		if (envoieToAPIYesBoolean != true && envoieToAPINoBoolean != true
				|| envoieToAPIYesBoolean == envoieToAPINoBoolean) {
			ajoutErreur("Envoie API", "Erreur un et seulement un des paramètres doit être coché");

		}

		// On vérifie que le chemin donner par l'utilisateur existe bien
		File dossierGraph = new File(recuperationChemin);
		if (!recuperationChemin.isEmpty()) {
			if (!dossierGraph.isDirectory()) {
				ajoutErreur("Chemin dossier", "Le chemin donné n'éxiste pas ou n'est pas un dossier");
			}
			if (!dossierGraph.canWrite()) {
				dossierGraph.setWritable(true);
				if (!dossierGraph.canWrite())
					ajoutErreur("Chemin dossier", "Vous n'avez pas les droits d'écrire dans le dossier");
			}
			if (this.erreur.isEmpty()) {
				affichageText.setText("Pas d'erreur");
			}
		} else {
			ajoutErreur("Chemin dossier", "Le chemin ne doit pas être nul");
		}
		if (this.erreur.isEmpty()) {
			affichageText.setText("Pas d'erreur");
			InterfaceGraphique.initialisation = true;
			System.out.println("boolean est a : " + InterfaceGraphique.initialisation);
			InterfaceGraphique.appGraph = new Application2(recuperationChemin, envoieToAPIYesBoolean,
					prettyPrintYesBoolean, dateS);

		}

		else {
			String sErreur = ToolsAffichage.affichageErreur(erreur);
			affichageText.setText(sErreur);
		}
		erreur.clear();

	}

	public HashMap<String, String> getErreur() {
		return erreur;
	}

	public void setErreur(HashMap<String, String> erreur) {
		this.erreur = erreur;
	}

	public void ajoutErreur(String erreur, String message) {
		this.erreur.put(erreur, message);
	}

}
