package fr.irisa.fluxcampus.MoteurGraph;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class InterfaceGraphique extends Application {
	public static boolean initialisation ;
	public static Application2 appGraph;
	private static Stage primaryStage;
	private static BorderPane rootLayout;

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Interface Graph");

		initRootLayout();
		showInitView();
		// showCreationGraphView();

	}

	/*
	 * Affiche la layout de base
	 */
	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InterfaceGraphique.class.getResource("../view/RootVue.fxml"));
			rootLayout = (BorderPane) loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
  

	public static void showInitView() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InterfaceGraphique.class.getResource("../view/RessourceView.fxml"));
			AnchorPane ressourceOverview = (AnchorPane) loader.load();
			// Show the scene containing the root layout.
			Scene scene = new Scene(ressourceOverview);
			rootLayout.setCenter(ressourceOverview);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void showGenerationGraphView() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InterfaceGraphique.class.getResource("../view/GenerationGraphView.fxml"));
			AnchorPane ressourceOverview = (AnchorPane) loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(ressourceOverview);
			rootLayout.setCenter(ressourceOverview);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		launch(args);
	}

}
