package fr.irisa.fluxcampus.MoteurGraph;

import javafx.fxml.FXML;

public class TopBarController {
	// Reference à l'application principale
	private static InterfaceGraphique mainApp;

	/*
	 * Initialise le contrôleur de classe,cette méthode est automatiquement appeler
	 * après que le fichier XML soit chargé.
	 */
	@FXML
	private void initialize() {

	}


	@FXML
	private void pressOnGenerationGraphButton()
	{
		InterfaceGraphique.showGenerationGraphView();
	}
	
	@FXML
	private void pressOnInformationButton()
	{

		InterfaceGraphique.showInitView();
	}
}
