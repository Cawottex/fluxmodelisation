package fr.irisa.fluxcampus.MoteurGraph;

import java.util.Date;

import org.neo4j.graphdb.Label;

import fr.irisa.fluxcampus.hierachie_universitaire.Campus;
import fr.irisa.fluxcampus.hierachie_universitaire.Formation;
import fr.irisa.fluxcampus.hierachie_universitaire.Promotion;

public enum TypeDeNoeud implements Label {
FORMATION(null),
PROMOTION(null),
DATE(null),
CAMPUS(null),
AMPHITHEATRE("CM"),
MAJ("Mise a Niveau"),
TD("TD"),
SORTIE("SORTIE"),
CC("Controle"),
PROJET("Projet"),
TP("TP");
	
	private String name;
	
	
	TypeDeNoeud(String n)
	{
		setName(n);
	}
	

	
	public static final TypeDeNoeud  createTypeDeNoeud(Formation f)
	{
		TypeDeNoeud typeFormation = TypeDeNoeud.FORMATION;
		typeFormation.setName(f.getNom());
		return typeFormation;
	}
	
	public static final TypeDeNoeud  createTypeDeNoeud(Campus c)
	{
		TypeDeNoeud typeCampus = TypeDeNoeud.CAMPUS;
		typeCampus.setName(c.getName());
		return typeCampus;
	}
	

	public static final TypeDeNoeud  createTypeDeNoeud(Promotion p)
	{
		TypeDeNoeud typePromotion = TypeDeNoeud.PROMOTION;
		typePromotion.setName(p.getName());
		return typePromotion;
	}
	
	public static final TypeDeNoeud createTypeDeNoeud(String type)
	{
		if(type.equals("CM"))
		{
			TypeDeNoeud typeCM = TypeDeNoeud.AMPHITHEATRE;
			return typeCM;
		}
		else if(type.equals("TD"))
		{
			TypeDeNoeud typeTD = TypeDeNoeud.TD;
			return typeTD;
		}
		
		else if(type.equals("TP"))
		{
			TypeDeNoeud typeTP = TypeDeNoeud.TP;
			return typeTP;
		}
		else if(type.equals("MAJ"))
		{
			TypeDeNoeud typeMAJ = TypeDeNoeud.MAJ;
			return typeMAJ;
		}
		else if(type.equals("CC"))
		{
			TypeDeNoeud typeControle = TypeDeNoeud.CC;
			return typeControle;
		}
		else if(type.equals("PROJET"))
		{
			TypeDeNoeud typeProjet = TypeDeNoeud.PROJET;
			return typeProjet;
		}
		else
		{
			System.err.println("Erreur type :" + type + " non connue");
			return null;
		}
	}
	
	public static final TypeDeNoeud createTypeDifferents(String typeDifferents)
	{

		if(typeDifferents.equals("TiP"))
		{
			TypeDeNoeud typeTP = TypeDeNoeud.TP;
			return typeTP;
		}
		if(typeDifferents.equals("MAT1"))
		{
			TypeDeNoeud typeTP = TypeDeNoeud.TP;
			return typeTP;
		}
		if(typeDifferents.equals("PSVCM"))
		{
			TypeDeNoeud typeTP = TypeDeNoeud.AMPHITHEATRE;
			return typeTP;
		}
		if(typeDifferents.equals("TiD"))
		{
			TypeDeNoeud typeTP = TypeDeNoeud.TD;
			return typeTP;
		}
		if(typeDifferents.equals("TID"))
		{
			TypeDeNoeud typeTP = TypeDeNoeud.TD;
			return typeTP;
		}
		return null;
		
	}
	
	public static boolean typeDifferent(String type)
	{
		if(type.equals("TiP"))
		{
			return true;
		}
		if(type.equals("MAT1"))
		{
			return true;
		}
		if(type.equals("PSVCM"))
		{
			return true;
		}
		if(type.equals("TiD"))
			return true;
		if(type.equals("TID"))
			return true;
		return false;
	}



	public static final TypeDeNoeud createTypeDeNoeud(Date d)
	{
		TypeDeNoeud typeDate = TypeDeNoeud.DATE;
		typeDate.setName(d.toString());
		return typeDate;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString()
	{
		return name;
	}
}
