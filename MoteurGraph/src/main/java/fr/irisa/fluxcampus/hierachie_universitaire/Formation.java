package fr.irisa.fluxcampus.hierachie_universitaire;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.graphdb.Node;

import fr.irisa.fluxcampus.MoteurGraph.TransactionGraph;
import fr.irisa.fluxcampus.MoteurGraph.TypeDeNoeud;
import fr.irisa.fluxcampus.interfaceAPI.RecuperationInformation;

public class Formation {
	private ArrayList<Promotion> listeDesPromotionDeLaFormation = new ArrayList<Promotion>();
	private static final Logger LOG = LogManager.getLogger(Formation.class);
	private String nom;
	private String nomFormation;
	private boolean noeudFormationCreer = false;
	private TypeDeNoeud typeFormation;
	private TransactionGraph graph;
	private Node noeud;
	
	
	public Formation(String nomFormation,TransactionGraph graph)
	{
		LOG.debug("Creation d'une formation, nom : " + nomFormation);
		setNom(nomFormation);
		this.graph = graph;
	}
	
	public void addPromotionInList(Promotion p)
	{
		LOG.debug("rajout d'une promotion methode :addPromotionInList in Formation.class ");
		if(!p.existInList(listeDesPromotionDeLaFormation))
		{
			LOG.debug("Promotion bien rajouté");
			listeDesPromotionDeLaFormation.add(p);
		}
		else
		{
			LOG.debug("Promotion déjà existante");
		}
	}
	
	
	public ArrayList<Promotion> getListeDesPromotionDeLaFormation() {
		return listeDesPromotionDeLaFormation;
	}
	public void setListeDesPromotionDeLaFormation(ArrayList<Promotion> listeDesPromotionDeLaFormation) {
		this.listeDesPromotionDeLaFormation = listeDesPromotionDeLaFormation;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public void addPromotion(String fichier,String anneeScolaire,String name)
	{
		if(!Promotion.existInList(listeDesPromotionDeLaFormation, fichier, anneeScolaire))
		{
			Promotion p = new Promotion(name,fichier,anneeScolaire,graph);
			listeDesPromotionDeLaFormation.add(p);
			
		}
	}
	
	public static final boolean existInList(ArrayList<Formation> liste , String nomFormation){
		if(liste.isEmpty())
			return false;
		for(Formation f : liste)
		{
			if(f.getNom().equals(nomFormation))
				return true;
		}
		return false;
	}
	
	public static final Formation RecupererFormationParNom(ArrayList<Formation> liste , String nomFormation){
		for(Formation f : liste)
		{
			if(f.getNom().equals(nomFormation))
				return f;
		}
		//Si on arrive a cette ligne c'est que l'on n'a pas vérifier la présence de la formation dans la liste
		return null;
	}
	
	
	
	public void recupererInformation(String date)
	{
		for(Promotion p :listeDesPromotionDeLaFormation )
		{
			p.recupererInformation(this.nom,date);
		}
	}
	/*
	 * Créer le graph, cette méthode créer un noeud qui représente la formation elle même
	 * puis appel pour chaqune de ses promotions la methode creationGraph qui a pour but de modéliser la promotion
	 * et l'emploie du temps
	 */
	
	public void creationGraph(Node nodeCampus)
	{
		LOG.debug("Creation Graph call pour la formation : " + this.getNom());
		//Noeud représentant la formation
		if(!noeudFormationCreer)
		{
			typeFormation = TypeDeNoeud.createTypeDeNoeud(this);
			noeudFormationCreer = true;
			noeud = graph.addNodeFormation(typeFormation,this,this.nomFormation);
			graph.creationRelationContains(nodeCampus, noeud);
		}
		//Appel de méthode pour toute les promotions appartenant à la formation
		for(Promotion p : listeDesPromotionDeLaFormation)
		{
			p.creationGraph(noeud);
		}
		
	}
	
	public void creeAgenda()
	{
		for(Promotion p :listeDesPromotionDeLaFormation )
		{
			p.creeAgenda();
		}
	}
	
	
	public void afficherAgenda()
	{
		for(Promotion p :listeDesPromotionDeLaFormation )
		{
			p.afficherAgenda();
		}
	}

	public void trier() {
		
		for(Promotion p :listeDesPromotionDeLaFormation )
		{
			p.trier();
		}
	}

	public void recupererInformationNiveau(String fichierAvecExtension) {
		model.getInfo.getInfo information = RecuperationInformation.getInformationNiveau(fichierAvecExtension);
		nomFormation = information.getFormation();
		
		for(Promotion p :listeDesPromotionDeLaFormation)
			p.recupererInformationNiveau(information);
		
	}

}
