package fr.irisa.fluxcampus.hierachie_universitaire;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.graphdb.Node;

import fr.irisa.fluxcampus.MoteurGraph.TransactionGraph;
import fr.irisa.fluxcampus.MoteurGraph.TypeDeNoeud;
import fr.irisa.fluxcampus.interfaceAPI.RecuperationInformation;
import fr.irisa.fluxcampus.structure.FichierBdd;

public class Campus {
	private static final Logger LOG = LogManager.getLogger(Campus.class);

	private String name;
	private ArrayList<Formation> listeDesFormations = new ArrayList<Formation>();
	private TransactionGraph transaction;
	private Node nodeCampus;

	public Campus(String name, TransactionGraph t) {
		this.name = name;
		setTransaction(t);

	}

	public void rajouterPromotion(FichierBdd fichier)
	{
		//Si la formation n'existe pas alors on rajoute la formation et la promotion
		if(!Formation.existInList(listeDesFormations, fichier.getFormation()))
		{
			Formation newFormation = new Formation(fichier.getFormation(),transaction);
			Promotion newPromotion = new Promotion(fichier.getPromo(),fichier.getName(),fichier.getAnnee_scolaire(),transaction);
			newFormation.addPromotionInList(newPromotion);
			listeDesFormations.add(newFormation);
		
		}
		else
		{
			//On sait que la formation existe,
			//On va rechercher si la promotion est déjà existante dans la formation
			Formation formationCorrespondante = Formation.RecupererFormationParNom(listeDesFormations, fichier.getFormation());
			//On a recuperer la formation correspondante
			//Maintenant on cherche a savoir si la promotion existe ou non dans la formation
			//Si oui on ne fait rien, si non on la rajoute
			if (!Promotion.existInList(formationCorrespondante.getListeDesPromotionDeLaFormation(), fichier.getPromo(),fichier.getAnnee_scolaire()))
			{
				//La promoton n'existe pas, on la rajoute
				Promotion newPromo = new Promotion(fichier.getPromo(),fichier.getName(),fichier.getAnnee_scolaire(),transaction);
				formationCorrespondante.addPromotionInList(newPromo);
			}
		}
	}


	public void addFormation(Formation f) {
		listeDesFormations.add(f);
	}

	public void addFormation(String nomFormation) {
		Formation f = new Formation(nomFormation, transaction);
		listeDesFormations.add(f);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Formation> getListeDesFormations() {
		return listeDesFormations;
	}

	public void setListeDesFormations(ArrayList<Formation> listeDesFormations) {
		this.listeDesFormations = listeDesFormations;
	}

	public TransactionGraph getTransaction() {
		return transaction;
	}

	public void setTransaction(TransactionGraph transaction) {
		this.transaction = transaction;
	}
	
	public void recupererInformation(String date)
	{
		for(Formation f: listeDesFormations)
		{
			f.recupererInformation(date);
		}
	}
	
	public void creeAgenda()
	{
		for(Formation f : listeDesFormations)
		{
			f.creeAgenda();
			f.trier();
		}
	}

	public void creationGraph() {
		LOG.debug("Creation Graph call pour le Campis : " + this.name);
		//Noeud représentant la formation
		
			TypeDeNoeud campusType  = TypeDeNoeud.createTypeDeNoeud(this);
			nodeCampus = transaction.addNodeCampus(campusType,this);

		//Appel de méthode pour toute les promotions appartenant à la formation
		for(Formation f : listeDesFormations)
		{
			f.creationGraph(nodeCampus);
		}
		
		
	}

	public void recupererInformationNiveau(String fichierAvecExtension) {
		for(Formation f : listeDesFormations)
			f.recupererInformationNiveau(fichierAvecExtension);
		
	}

}
