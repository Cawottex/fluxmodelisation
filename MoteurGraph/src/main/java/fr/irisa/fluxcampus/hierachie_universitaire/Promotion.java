package fr.irisa.fluxcampus.hierachie_universitaire;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.graphdb.Node;

import fr.irisa.fluxcampus.MoteurGraph.TransactionGraph;
import fr.irisa.fluxcampus.MoteurGraph.TypeDeNoeud;
import fr.irisa.fluxcampus.agenda.Agenda;
import fr.irisa.fluxcampus.interfaceAPI.RecuperationInformation;
import model.getInfo.getInfo;

public class Promotion {

	private String name;
	private ArrayList<model.getInfoUE.InfoUE> listeInformationUE = new ArrayList<model.getInfoUE.InfoUE>();
	private int nombre_etudiant;
	private String annee_scolaire;
	private String fichier;
	private Node noeud;
	private String sNiveau;
	private String nomPromotion;
	private boolean noeudFormationCreer = false;
	private ArrayList<model.getEvenNonParse.EvenementParse> ListeEvenement = new ArrayList<model.getEvenNonParse.EvenementParse>();
	private static final Logger LOG = LogManager.getLogger(Promotion.class);
	private TypeDeNoeud typePromotion;
	private TransactionGraph graph;

	private ArrayList<model.getGroupeByPromoAndFormation.GetGroupeByPromoAndFormation> listesDesGroupes = new ArrayList<model.getGroupeByPromoAndFormation.GetGroupeByPromoAndFormation>();
	
	/*
	getGroupeByPromoAndFormation(
			String promo, String formation) {
	
	
	getGroupeByPromoAndFormation(
			String promo, String formation) {>
			*/
	private ArrayList<model.libellecours.LibelleCours> listeLibelleCours= new ArrayList<model.libellecours.LibelleCours>();
	private boolean agendaCree = false;
	private Agenda agendaPromotion = null;

	public Promotion(String name, String fichier, String annee_scolaire, TransactionGraph graph) {
		this.setName(name);
		this.annee_scolaire = annee_scolaire;
		this.fichier = fichier;
		this.graph = graph;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TransactionGraph getGraph()
	{
		return graph;
	}
	public boolean existInList(ArrayList<Promotion> listePromotion) {
		for (Promotion p : listePromotion) {
			if (p.getName().equals(this.getName()))
				return true;
		}
		return false;
	}

	public ArrayList<model.getInfoUE.InfoUE> getListeInformationUE() {
		return listeInformationUE;
	}

	public void setListeInformationUE(ArrayList<model.getInfoUE.InfoUE> listeInformationUE) {
		this.listeInformationUE = listeInformationUE;
	}

	public int getNombre_etudiant() {
		return nombre_etudiant;
	}

	public void setNombre_etudiant(int nombre_etudiant) {
		this.nombre_etudiant = nombre_etudiant;
	}

	public ArrayList<model.getEvenNonParse.EvenementParse> getListeEvenement() {
		return ListeEvenement;
	}

	public void setListeEvenement(ArrayList<model.getEvenNonParse.EvenementParse> listeEvenement) {
		ListeEvenement = listeEvenement;
	}

	public Agenda getAgendaPromotion() {
		return agendaPromotion;
	}

	public void setAgendaPromotion(Agenda agendaPromotion) {
		this.agendaPromotion = agendaPromotion;
	}
	
	public Node getNode()
	{
		return noeud;
	}

	public boolean isAgendaCree() {
		return agendaCree;
	}

	public void setAgendaCree(boolean agendaCree) {
		this.agendaCree = agendaCree;
	}

	public void recupererInformation(String formation,String date) {
		LOG.debug("Recuperation information lancé pour la promotion " + this.name);
		nombre_etudiant = RecuperationInformation.getNombreEtudiant(fichier);
		LOG.debug("nombre d'étudiant récupèrer : " + nombre_etudiant);
		listeInformationUE = RecuperationInformation.getInformationSurUE(fichier);
		LOG.debug("Liste information ue  taille : " + listeInformationUE.size());
		System.out.println("Liste information ue  taille : " + listeInformationUE.size());

		ListeEvenement = RecuperationInformation.getEvenementParse(fichier, annee_scolaire,date);
		LOG.debug("Taille de la liste des évènements : " + ListeEvenement.size());
		setListeLibelleCours(RecuperationInformation.getLibelleParTag(fichier));
		LOG.debug("Taille de la liste des libelles : " + listeLibelleCours.size());
		System.out.println("Taille de la liste des libelles : " + listeLibelleCours.size());
		setListesDesGroupes((RecuperationInformation.getGroupe(this.name, formation)));
		LOG.debug("Taille de la liste des groupes : " + listesDesGroupes.size());
		System.out.println("Taille de la liste des groupes : " + listesDesGroupes.size());
		for(model.getGroupeByPromoAndFormation.GetGroupeByPromoAndFormation g : listesDesGroupes)
		{
			System.out.println(g.toString());
		}

	}

	public String getFichier() {
		return fichier;
	}

	public String getAnneeScolaire() {
		return this.annee_scolaire;
	}

	public static final boolean existInList(ArrayList<Promotion> liste, String fichier, String annee_scolaire) {
		for (Promotion p : liste) {
			if (p.getFichier().equals(fichier) && p.getAnneeScolaire().equals(annee_scolaire))
				return true;
		}
		return false;
	}

	public void creeAgenda() {

		if (!agendaCree) {
			agendaPromotion = new Agenda(this);
			agendaCree = true;
			System.err.println("taille liste parse   : " + ListeEvenement.size());
			for (model.getEvenNonParse.EvenementParse e : ListeEvenement) {
				agendaPromotion.insererCours(e);
			}
		}
	}

	public void creationGraph(Node noeudFormation) {
		LOG.debug("Creation Graph call pour la promotion : " + this.getName());
		if (!noeudFormationCreer) {
			typePromotion = TypeDeNoeud.createTypeDeNoeud(this);
			noeudFormationCreer = true;
			noeud = graph.addNodePromotion(typePromotion, this);
			// On relie le noeud de la formation a celui-ci
			graph.creationRelationContains(noeudFormation, noeud);
			agendaPromotion.creationGraph();
			}

	}

	// Affiche l'agenda
	public void afficherAgenda() {
		// On regarde si celui-ci est différent de null dans a étais crée
		LOG.debug("affichage de l'agenda pour la promotion : " +  this.getName());
		System.out.println("affichage Agenda pour la promotion " + this.getName());
		if (agendaCree) {
			LOG.debug("agenda non null");
			agendaPromotion.afficherAgenda();
		}
		else
		{
			LOG.debug("agenda null echec de l'affichage");
		}

	}

	public void trier() {
		if (agendaCree) {
			agendaPromotion.trier();
		}
		
	}

	public ArrayList<model.libellecours.LibelleCours> getListeLibelleCours() {
		return listeLibelleCours;
	}

	public void setListeLibelleCours(ArrayList<model.libellecours.LibelleCours> listeLibelleCours) {
		this.listeLibelleCours = listeLibelleCours;
	}

	public ArrayList<model.getGroupeByPromoAndFormation.GetGroupeByPromoAndFormation> getListesDesGroupes() {
		return listesDesGroupes;
	}

	public void setListesDesGroupes(ArrayList<model.getGroupeByPromoAndFormation.GetGroupeByPromoAndFormation> listesDesGroupes) {
		this.listesDesGroupes = listesDesGroupes;
	}
	
	public int getNombrePersonneDansGroupe(String nomGroupe)
	{
		System.err.println("Type a cherche : " + nomGroupe);
		for(model.getGroupeByPromoAndFormation.GetGroupeByPromoAndFormation g: listesDesGroupes)
		{
			System.err.println("compare avec  : " + g.getGroupe());

			if(g.getGroupe().equals(nomGroupe))
			{
				System.err.println("comparaison ok : nombre etudiant :"  + g.getEffectif());
				return g.getEffectif();
			}
		}
		return -1;
	}

	public void recupererInformationNiveau(getInfo information) {
		setsNiveau(information.getNiveau());
		setNomPromotion(information.getPromotion());
		
	}

	public String getsNiveau() {
		return sNiveau;
	}

	public void setsNiveau(String sNiveau) {
		this.sNiveau = sNiveau;
	}

	public String getNomPromotion() {
		return nomPromotion;
	}

	public void setNomPromotion(String nomPromotion) {
		this.nomPromotion = nomPromotion;
	}
}
